build and install
```sh
make
sudo make install
```

Arch Linux PKGBUILD: <https://git.flylightning.xyz/fly/tree/pkgbuilds/xcross-git/PKGBUILD?id=b284d75ff70139672727f6ead90587ed6aa65630>

I don't think anyone else will use this program, so I didn't put the PKGBUILD in AUR. Request it if you want.
